(function() {
  'use strict';

  angular
    .module('myPlace.Controllers')
    .controller('cajaController', cajaController);

  cajaController.$inject = [];

  function cajaController() {
    var ctrl = this;

    ctrl.myCaja = "I'm an injected state";
  }
})();
