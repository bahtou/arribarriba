(function() {
  'use strict';

  angular
    .module('myPlace', [
      'routes',
      'myPlace.Controllers',
      'cajaComponent'
    ]);

  angular.module('routes', ['ui.router']);
  angular.module('myPlace.Controllers', []);
  angular.module('cajaComponent', []);

})();
