(function() {
  'use strict';

  /*
   * Every state will inherit the header, nav and footer
   * another way of doing this is via the 'parent' key
   */
  var _root = {
   caja: 'root.caja'
  };

  angular
    .module('routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    //state list
    $stateProvider
      .state('root', {
        url: '',
        abstract: true,
        views: {
          'header': {
            templateUrl: 'layout/header/header.html',
            controller: 'headerController',
            controllerAs: 'ctrl'
          },
          'nav': {
            templateUrl: 'layout/nav/nav.html',
            controller: 'navController',
            controllerAs: 'ctrl'
          },
          'footer': {
            templateUrl: 'layout/footer/footer.html',
            controller: 'footerController',
            controllerAs: 'ctrl'
          }
        }
      })
      .state(_root.caja, {
        // parent: 'root',
        url: '/',
        views: {
          '@': {
            templateUrl: 'layout/unnamedState.html'
          },
          'main@': {
            templateUrl: 'components/caja/caja.html',
            controller: 'cajaController',
            controllerAs: 'ctrl'
          },
          '@root.caja': {
            template: '<h3>Left Column</h3>'
          }
        }
      })
      .state(_root.caja + '.columnLeft', {
        template: '<h3>Left Column</h3>'
      })
      .state(_root.caja + '.columnRight', {
        template: '<h3>Right Column</h3>'
      });

    //fall through
    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: true,
      rewriteLinks: false
    });
  }

})();
