(function() {
  'use strict';

  angular
    .module('myPlace.Controllers')
    .controller('navController', navController);

  navController.$inject = [];

  function navController() {
    var ctrl = this;

    ctrl.myNav = 'controller says: I am the navigation controller';
  }
})();
