(function() {
  'use strict';

  angular
    .module('myPlace.Controllers')
    .controller('footerController', footerController);

  footerController.$inject = [];

  function footerController() {
    var ctrl = this;

    ctrl.myFooter = 'controller says: I am the footer controller';
  }
})();
